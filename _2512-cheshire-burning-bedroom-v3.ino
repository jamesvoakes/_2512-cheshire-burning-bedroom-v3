//***************************************//
// PARAGON CREATIVE - BURNING BEDROOM v3 //
// Cheshire Safety Centre                //
// 2017                                  //
//***************************************//

#include <Controllino.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "timedFlicker.h"
#include "timedRamp.h"
#include <utility/w5100.h>

// Setup Ethernet MAC and IP Address
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
unsigned int localPort = 8888;
EthernetUDP Udp;

// Setup ESA Cue defs
IPAddress ESA_IP(10,0,0,42);
int ESA_PORT = 2430;
byte Cue1[] = {0x53,0x69,0x75,0x64,0x69,0x5f,0x37,0x42,0x6d,0x00,0x01,0x01,0x00,0x00,0x01};
byte Cue2[] = {0x53,0x69,0x75,0x64,0x69,0x5f,0x37,0x42,0x6d,0x00,0x02,0x01,0x00,0x00,0x01};
byte Cue3[] = {0x53,0x69,0x75,0x64,0x69,0x5f,0x37,0x42,0x6d,0x00,0x03,0x01,0x00,0x00,0x01};

// Set Brightsign Audio UDP Cues and Settings
IPAddress brightsign_ip(10,0,0,55);
int brightsign_port = 8893;
char startAudio[] = "bb_bs_sfx1_start";
char stopAudio[] = "bb_bs_sfx1_stop";

// Set Brightsign Video UDP Cues and settings
IPAddress bsV_ip(10,0,0,56);
int bsV_port = 8893;
char startVideo[] = "play";
char pauseVideo[] = "pause";
char stopVideo[] = "stop";

// Set IO Definitions
#define Atomiser1   CONTROLLINO_R0    // Relay  - Top Tank Atomiser 1
#define Atomiser2   CONTROLLINO_R1    // Relay  - Top Tank Atomiser 2
#define Atomiser3   CONTROLLINO_R2    // Relay  - Top Tank Atomiser 3
#define Atomiser4   CONTROLLINO_R3    // Relay  - Top Tank Atomiser 4
#define Fan1        CONTROLLINO_D0    // PWM    - Top Tank Fans 1 (5V Logic)
#define Fan2        CONTROLLINO_D1    // PWM    - Top Tank Fans 2 (5V Logic)
#define Atomiser5   CONTROLLINO_R13   // Relay  - Bottom Tank Atomiser 1
#define Atomiser6   CONTROLLINO_R12   // Relay  - Bottom Tank Atomiser 2
#define Fan3        CONTROLLINO_D2    // PWM    - Bottom Tank Fans 1 (5V Logic)
#define Fan4        CONTROLLINO_D3    // PWM    - Bottom Tank Fans 2 (5V Logic)
#define Atomiser9   CONTROLLINO_R8    // Relay  - Bed Tank Atomiser 1
#define Fan5        CONTROLLINO_D5    // PWM    - Bed Tank Fan 1 (5V Logic)
#define Atomiser10  CONTROLLINO_R9    // Relay  - Laptop Atomiser 1
#define Fan6        CONTROLLINO_D4    // PWM    - Laptop Fan 1 (5V Logic)
#define IR1         CONTROLLINO_R6    // Relay  - Infrared Heater SSRelay
#define SC1         CONTROLLINO_R14   // Relay  - Scent Cannon
#define HUM1        CONTROLLINO_R4    // Relay  - Dehumidifiers SSRelay
#define PROJECTOR   CONTROLLINO_R5    // Relay  - Projector SSRelay
#define LED1        CONTROLLINO_D6    // FastLED WS2801 Data--scrap that, flashover trigger now
#define LED2        CONTROLLINO_D7    // FastLED WS2801 Clock--scrap that, emergency lighting now
#define LED3        CONTROLLINO_D8    // Sparking Laptop LED1
#define LED4        CONTROLLINO_D9    // Sparking Laptop LED2
#define LED5        CONTROLLINO_D10   // Sparking Laptop LED3
#define MANSTART    CONTROLLINO_A0    // Manual Start Button for show
#define MANSTOP     CONTROLLINO_A1    // Manual Stop Button for show
#define DOORTRIG    CONTROLLINO_A2    // Door Micro Switch input to trigger flashover
#define BLAMPS1     CONTROLLINO_D13   // Bedside lamp digital output (12V High power)
#define BLAMPS2     CONTROLLINO_D12   // Bedside lamp digital output (12V High power)
#define BLight1     CONTROLLINO_D11   // In bed halogens
#define BLight2     CONTROLLINO_D14   // In bed halogens
#define BLight3     CONTROLLINO_D15   // In bed halogens
#define startLed    CONTROLLINO_D16   // Start Button LED
#define stopLed     CONTROLLINO_D17   // Stop Button LED
#define readyLed    CONTROLLINO_D18   // Flashover ready to go LED


// Global Variables
int allOutputs[10] = {Atomiser1, Atomiser2, Atomiser3, Atomiser4, Atomiser5, Atomiser6, Atomiser9, Atomiser10, IR1, SC1};
int showTimer = -1;
unsigned long previousMillis = 0;
unsigned long previousShortMillis = 0;
unsigned long previousShortMillis2 = 0;
timedFlicker bedLamp1(BLAMPS1);
timedFlicker bedLamp2(BLAMPS2);
timedFlicker laptopFan(Fan6);
timedRamp bottomFan1(Fan3);
timedRamp bottomFan2(Fan4);
timedRamp bedFan(Fan5);
timedRamp bedLight1(BLight1);
timedRamp bedLight2(BLight2);
timedRamp bedLight3(BLight3);
timedFlicker laptopLed1(LED3);
timedFlicker laptopLed2(LED4);
//char * recvd[UDP_TX_PACKET_MAX_SIZE];
char recvd[UDP_TX_PACKET_MAX_SIZE];
int packetSize = 0;

void setup() {
  // Set defaults at startup
  randomSeed(analogRead(0));
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  digitalWrite(LED1, HIGH);
  digitalWrite(LED2, HIGH);

  // Start diagnostic Serial
  Serial.begin(9600);
  
  // Start the Ethernet and UDP:
  Ethernet.begin(mac);
  Udp.setTimeout(1000);
  W5100.setRetransmissionTime(0x01F4);
  W5100.setRetransmissionCount(2);
  Udp.begin(localPort);

  // Setup Timer0 Interrupt
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A);

  digitalWrite(stopLed, HIGH);
  Serial.println(Ethernet.localIP());
}

void loop() {
  if ((showTimer==-2) and (digitalRead(DOORTRIG)==LOW)) { startShow(249); }
  delay(10);
  packetSize = Udp.parsePacket();
  if (packetSize) { 
    for( int i = 0; i < UDP_TX_PACKET_MAX_SIZE;  ++i ) { recvd[i] = (char)0; }
    Udp.read(recvd, UDP_TX_PACKET_MAX_SIZE);
    Serial.println(recvd);

    if (strcmp(recvd, "bbAdultShow")==0) { sendUdpWithIP("startShow!", Udp.remoteIP(), Udp.remotePort()); startShow(0); }
    if (strcmp(recvd, "bbChildShow")==0) { sendUdpWithIP("startShow!", Udp.remoteIP(), Udp.remotePort()); startShow(0); }
    if (strcmp(recvd, "bbStopShow")==0) { sendUdpWithIP("stopShow!", Udp.remoteIP(), Udp.remotePort()); stopShow(); }
    if (strcmp(recvd, "startBedA")==0) { sendUdpWithIP("startBedA!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Atomiser9, HIGH); }
    if (strcmp(recvd, "stopBedA")==0) { sendUdpWithIP("stopBedA!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Atomiser9, LOW); }
    if (strcmp(recvd, "startLaptopA")==0) { sendUdpWithIP("startLaptopA!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Atomiser10, HIGH); }
    if (strcmp(recvd, "stopLaptopA")==0) { sendUdpWithIP("stopLaptopA!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Atomiser10, LOW); }
    if (strcmp(recvd, "startBedL")==0) { sendUdpWithIP("startBedL!", Udp.remoteIP(), Udp.remotePort()); onOff((const int[]) {BLight1, BLight2, BLight3}, 3, HIGH); }
    if (strcmp(recvd, "stopBedL")==0) { sendUdpWithIP("stopBedL!", Udp.remoteIP(), Udp.remotePort()); onOff((const int[]) {BLight1, BLight2, BLight3}, 3, LOW); }
    if (strcmp(recvd, "startFlashover")==0) {sendUdpWithIP("startFlashover!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(LED1, LOW); }
    if (strcmp(recvd, "stopFlashover")==0) {sendUdpWithIP("stopFlashover!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(LED1, HIGH); }
    if (strcmp(recvd, "startBTankA")==0) { sendUdpWithIP("startBTankA!", Udp.remoteIP(), Udp.remotePort()); onOff((const int[]) {Atomiser5, Atomiser6}, 2, HIGH); }
    if (strcmp(recvd, "stopBTankA")==0) { sendUdpWithIP("stopBTankA!", Udp.remoteIP(), Udp.remotePort()); onOff((const int[]) {Atomiser5, Atomiser6}, 2, LOW); }
    if (strcmp(recvd, "startTTankA")==0) { sendUdpWithIP("startTTankA!", Udp.remoteIP(), Udp.remotePort()); onOff((const int[]) {Atomiser1, Atomiser2, Atomiser3, Atomiser4}, 4, HIGH); }
    if (strcmp(recvd, "stopTTankA")==0) { sendUdpWithIP("stopTTankA!", Udp.remoteIP(), Udp.remotePort()); onOff((const int[]) {Atomiser1, Atomiser2, Atomiser3, Atomiser4}, 4, LOW); }
    if (strcmp(recvd, "startHeaters")==0) { sendUdpWithIP("startHeaters!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(IR1, HIGH); }
    if (strcmp(recvd, "stopHeaters")==0) { sendUdpWithIP("stopHeaters!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(IR1, LOW); }
    if (strcmp(recvd, "startHums")==0) { sendUdpWithIP("startHums!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(HUM1, HIGH); }
    if (strcmp(recvd, "stopHums")==0) { sendUdpWithIP("stopHums!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(HUM1, LOW); }
    if (strcmp(recvd, "startBedF")==0) { sendUdpWithIP("startBedF!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Fan5, HIGH); }
    if (strcmp(recvd, "stopBedF")==0) { sendUdpWithIP("stopBedF!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Fan5, LOW); }
    if (strcmp(recvd, "startLaptopF")==0) { sendUdpWithIP("startLaptopF!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Fan6, HIGH); }
    if (strcmp(recvd, "stopLaptopF")==0) { sendUdpWithIP("stopLaptopF!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Fan6, LOW); }
    if (strcmp(recvd, "startDmxCue")==0) { sendUdpWithIP("startDmxCue!", Udp.remoteIP(), Udp.remotePort()); sendDmxCue(Cue3,ESA_IP, ESA_PORT); }
    if (strcmp(recvd, "stopDmxCue")==0) { sendUdpWithIP("stopDmxCue!", Udp.remoteIP(), Udp.remotePort()); sendDmxCue(Cue2,ESA_IP, ESA_PORT); }
    if (strcmp(recvd, "startBottomF")==0) { sendUdpWithIP("startBottomF!", Udp.remoteIP(), Udp.remotePort()); analogWrite(Fan3, 200); analogWrite(Fan4, 200); }
    if (strcmp(recvd, "stopBottomF")==0) { sendUdpWithIP("stopBottomF!", Udp.remoteIP(), Udp.remotePort()); analogWrite(Fan3, 0); analogWrite(Fan4, 0); }
    if (strcmp(recvd, "startTopF")==0) { sendUdpWithIP("startTopF!", Udp.remoteIP(), Udp.remotePort()); analogWrite(Fan1, 200); analogWrite(Fan2, 200); }
    if (strcmp(recvd, "stopTopF")==0) { sendUdpWithIP("stopTopF!", Udp.remoteIP(), Udp.remotePort()); digitalWrite(Fan1, LOW); digitalWrite(Fan2, LOW); }
    if (strcmp(recvd, "startBedLeds")==0) { sendUdpWithIP("startBedLeds!", Udp.remoteIP(), Udp.remotePort()); laptopLed1.setOnForTime(6000, true, 255); laptopLed2.setOnForTime(6000, true, 255); }
    if (strcmp(recvd, "stopBedLeds")==0) { sendUdpWithIP("stopBedLeds!", Udp.remoteIP(), Udp.remotePort()); laptopLed1.setOff(); laptopLed2.setOff(); }
  }

  
}

void sendUdpWithIP(char message[], IPAddress ip, int port) {
  Udp.beginPacket(ip, port);
  Udp.write(message);
  Udp.endPacket();
}

char * receiveUdpOld() {
  char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet
  int packetSize = Udp.parsePacket();
  //String output;
  if (packetSize) {
    // read the packet into packetBufffer
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);

    //output = packetBuffer;
    //output = output.substring(0,packetSize);
    //return output;
    return packetBuffer;
  } else {
    return "";
  }
}

void sendDmxCue(byte msg[], IPAddress ip, int port){
  Udp.beginPacket(ip, port);
  for (int byteIndex = 0; byteIndex < 15; byteIndex++) {
    Udp.write((byte)msg[byteIndex]);
  }
  Udp.endPacket();
}

void stopShow(){
  showTimer = -1; //reset the show timer
  
  onOff(allOutputs, 10, LOW);
  
  laptopFan.setOff();
  bottomFan1.setOff();
  bottomFan2.setOff();
  bedFan.setOff();
  bedLight1.setOff();
  bedLight2.setOff();
  bedLight3.setOff();
  laptopLed1.setOff();
  laptopLed2.setOff();
  
  digitalWrite(HUM1, HIGH);
  bedLamp1.setOn(255, true);
  bedLamp2.setOn(255, true);
  digitalWrite(readyLed, LOW);

  sendDmxCue(Cue1,ESA_IP, ESA_PORT);
  sendUdpWithIP(stopAudio, brightsign_ip, brightsign_port);
  sendUdpWithIP(stopVideo, bsV_ip, bsV_port);

  digitalWrite(startLed, LOW);
  digitalWrite(stopLed, HIGH);

  Serial.println("stopShow");
}

void startShow(int from){
  Serial.println("startShow");
  digitalWrite(HUM1, LOW);
  digitalWrite(startLed, HIGH);
  digitalWrite(stopLed, LOW);
  bedLamp1.setOn(255, true);
  bedLamp2.setOn(255, true);
  showTimer = from; //start show
}

void siteOpen(){
  digitalWrite(PROJECTOR, HIGH);
  digitalWrite(BLAMPS1, HIGH);
  digitalWrite(BLAMPS2, HIGH);
  digitalWrite(HUM1, HIGH);
  stopShow();
}

void siteClose() {
  digitalWrite(PROJECTOR, LOW);
  digitalWrite(BLAMPS1, LOW);
  digitalWrite(BLAMPS2, LOW);
  digitalWrite(HUM1, LOW);
}

void onOff(const int inputs[], int noOfInputs, int highOrLow){
  for (int a=0; a<noOfInputs; a++) {
    digitalWrite(inputs[a], highOrLow);
  }
}

SIGNAL(TIMER0_COMPA_vect) {
  unsigned long currentMillis = millis();
    if ((unsigned long)(currentMillis - previousShortMillis) >= 10) {
      laptopFan.check();
      bottomFan1.check();
      bottomFan2.check();
      bedFan.check();
      bedLight1.check();
      bedLight2.check();
      bedLight3.check();
      laptopLed1.check();
      laptopLed2.check();
      previousShortMillis = currentMillis;
    }

    if ((unsigned long)(currentMillis - previousShortMillis2) >= 70) {
      bedLamp1.check();
      bedLamp2.check();
      if ((digitalRead(MANSTART)) && (showTimer == -1)) { startShow(0); }
      if (digitalRead(MANSTOP)) { stopShow(); }
      if (digitalRead(LED1)==LOW) {digitalWrite(LED1,HIGH);}
      previousShortMillis2 = currentMillis;
    }
    
    if ((unsigned long)(currentMillis - previousMillis) >= 1000) {
      if (showTimer > -1) { 
        showTimer++;
        showSequence(showTimer);  
      }
      if ((showTimer > -1) || (showTimer == -2)) { 
        digitalWrite(startLed, !digitalRead(startLed)); 
      } else {
        digitalWrite(stopLed, !digitalRead(stopLed));
      }
      previousMillis = currentMillis;
    }
}

void showSequence(int seconds){
  Serial.println(seconds);
  switch (seconds) {
    case 1:
      sendUdpWithIP(startAudio, brightsign_ip, brightsign_port);
      sendUdpWithIP(startVideo, bsV_ip, bsV_port);
      sendDmxCue(Cue2,ESA_IP, ESA_PORT);
      digitalWrite(Atomiser10, HIGH);
      break;
    case 7:
      laptopFan.setOnForTime(5000, false, 255);
      break;
    case 26:
      laptopFan.setOnForTime(5000, false, 255);
      break;
    case 27:
      laptopLed2.setOnForTime(9000, true, 255);
      break;
    case 36:
      laptopFan.setOnForTime(5000, false, 255);
      bedLight2.rampOnForTime(6000, 1, 255, false);
      break;
    case 40:
      digitalWrite(Atomiser9, HIGH);
      laptopFan.setOn(60, true);
      break;
    case 42:
      bedLight2.rampOnForTime(5000, 255, 1, true);
      digitalWrite(Atomiser10, LOW);
      break;
    case 50:
      bedLight2.rampOnForTime(20000, 1, 255, false);
      break;
    case 80:
      bedLight1.rampOnForTime(20000, 1, 255, false);
      break;
    case 120:
      bedLight3.rampOnForTime(20000, 1, 255, false);
      onOff((const int[]) {Atomiser1, Atomiser2, Atomiser3, Atomiser4}, 4, HIGH);
      break;
    case 150:
      laptopFan.setOnForTime(5000,false,128);
      break;
    case 160:
      laptopFan.setOnForTime(20000, false, 255);
      break;
    case 179:
      bedFan.setOn(255, true);  //1s pulse to start it spinning
      break;
    case 180:
      bedFan.rampOnForTime(20000, 50, 153, false);
      break;
    case 195:
      digitalWrite(Atomiser9, LOW);
      break;
    case 200:
      bedLamp1.setOnForTime(30000, true, 255);
      digitalWrite(IR1, HIGH);
      break;
    case 210:
      bedLamp2.setOnForTime(25000, true, 255);
      sendDmxCue(Cue3,ESA_IP, ESA_PORT);
      break;
    case 215:
      laptopFan.setOnForTime(5000,false,200);
      break;
    case 216:
      bottomFan1.rampOnForTime(30000, 1, 170, false);
      bottomFan2.rampOnForTime(30000, 1, 170, false);
      onOff((const int[]) {Atomiser5, Atomiser6}, 2, HIGH);
      break;
    case 225:
      digitalWrite(Atomiser9, HIGH);
      break;
    case 248:
      onOff((const int[]) {Atomiser1, Atomiser2, Atomiser3, Atomiser4}, 4, HIGH);
      break;
    case 249:
      showTimer=-2;
      digitalWrite(readyLed, HIGH);
      break;
    case 250:
      bedLamp1.setOff();
      bedLamp2.setOff();
      digitalWrite(readyLed, LOW);
      analogWrite(Fan1, 255);
      analogWrite(Fan2, 255);
      digitalWrite(LED1, LOW);
      break;
    case 251:
      digitalWrite(LED1, HIGH);
      break;
    case 255:
      onOff((const int[]) {Fan1, Fan2, Fan3, Fan4, Fan5, Fan6}, 6, LOW);
      digitalWrite(IR1, LOW);
      onOff((const int[]) {Atomiser1, Atomiser2, Atomiser3}, 3, LOW);
      break;
    case 260:
      onOff((const int[]) {Atomiser4, Atomiser5, Atomiser6}, 3, LOW);
      sendDmxCue(Cue2,ESA_IP, ESA_PORT);
      sendUdpWithIP(stopAudio, brightsign_ip, brightsign_port);
      break;
    case 290:
      stopShow();
      break;
  }
}
