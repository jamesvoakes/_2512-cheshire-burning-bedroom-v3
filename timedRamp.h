class timedRamp
{
private:
 uint8_t _ledPin;
 long _turnOffTime;
 int _endPwm;
 float _currentPwm;
 float _rampDiff;
 int _startPwm;
 bool _offAtEnd;

public:
 timedRamp(uint8_t pin);
 void setOn(int pwm, boolean reset);
 void setOff();

 // Turn the led on for a given amount of time (relies on a call to check() in the main loop())
 void rampOnForTime(float p_millis, float startPwm, float endPwm, int offAtEnd);
 void check();
};

timedRamp::timedRamp(uint8_t ledPin) : _ledPin(ledPin), _turnOffTime(0)
{
 pinMode(_ledPin, OUTPUT);
}

void timedRamp::setOn(int pwm, boolean reset)
{
  if (reset) { _turnOffTime = 0; }
  analogWrite(_ledPin, pwm);
}

void timedRamp::setOff()
{
  _turnOffTime = 0;
  _startPwm = 0;
  _endPwm = 0;
  analogWrite(_ledPin, 0);
}

void timedRamp::rampOnForTime(float p_millis, float startPwm, float endPwm, int offAtEnd)
{
 _turnOffTime = millis() + p_millis;
 if (offAtEnd > -1) {_turnOffTime = _turnOffTime + offAtEnd;}
 _endPwm = endPwm;
 _startPwm = startPwm;
 _currentPwm = startPwm;
 _rampDiff = (endPwm - startPwm) / (p_millis / 10.0000);
 _offAtEnd = offAtEnd;
 setOn(startPwm, false);
}

void timedRamp::check() {
  int pwm;
 if (_turnOffTime > 0 && (millis() > _turnOffTime)) {
    _turnOffTime = 0;
    if (_offAtEnd) { setOff(); }
  } else if(_turnOffTime != 0) {
    if ((_endPwm < _startPwm) || (_endPwm > _startPwm)) {
      _currentPwm = _currentPwm + _rampDiff;
      if (((_endPwm < _startPwm) && (_currentPwm < _endPwm)) || ((_endPwm > _startPwm) && (_currentPwm > _endPwm))) { _currentPwm = _endPwm; }
      setOn(_currentPwm, false);
    }
  }
}
