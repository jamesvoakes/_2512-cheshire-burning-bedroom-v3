class timedFlicker
{
private:
 uint8_t _ledPin;
 long _turnOffTime;
 byte flicker[13] = {180, 30, 89, 23, 255, 200, 90, 150, 60, 230, 180, 45, 90};
 boolean _flicker;

public:
 timedFlicker(uint8_t pin);
 void setOn(int pwm, boolean reset);
 void setOff();

 // Turn the led on for a given amount of time (relies on a call to check() in the main loop())
 void setOnForTime(int p_millis, boolean flicker, int pwm);
 void check();
};

timedFlicker::timedFlicker(uint8_t ledPin) : _ledPin(ledPin), _turnOffTime(0)
{
 pinMode(_ledPin, OUTPUT);
}

void timedFlicker::setOn(int pwm, boolean reset)
{
  if (reset) { _turnOffTime = 0, _flicker = false; }
  analogWrite(_ledPin, pwm);
}

void timedFlicker::setOff()
{
  _turnOffTime = 0;
  _flicker = false;
  digitalWrite(_ledPin, LOW);
}

void timedFlicker::setOnForTime(int p_millis, boolean flicker, int pwm)
{
 _turnOffTime = millis() + p_millis;
 _flicker = flicker;
 setOn(pwm, false);
}

void timedFlicker::check()
{
 if (_turnOffTime != 0 && (millis() > _turnOffTime))
 {
  digitalWrite(_ledPin, LOW);
  _turnOffTime = 0;
 } else if(_turnOffTime != 0) {
  int randPwm = flicker[random(0,13)];
  if (!_flicker) { randPwm = 255; }
  analogWrite(_ledPin, randPwm);
 }
}
